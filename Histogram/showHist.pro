#-------------------------------------------------
#
# Project created by QtCreator 2015-10-31T17:27:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = showHist
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp\
        histogramwidget.cpp

HEADERS  += mainwindow.h histogramwidget.h

FORMS    += mainwindow.ui

LIBS += -lsndfile
