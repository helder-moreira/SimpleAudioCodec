#include "mainwindow.h"
#include <QApplication>
#include <QColor>
#include <QFile>
#include <QFileInfo>
#include <sndfile.h>
#include <cmath>

#define MAX_SHORT 65536
#define HALF_SHORT MAX_SHORT/2

using namespace std;

int main(int argc, char *argv[]) {

    if (argc < 2 || argc > 3){
        fprintf(stderr, "Usage: <file.wav> [join_#_LSBits]\n");
        return 1;
    }

    if (!QFile(argv[1]).exists()) {
        fprintf(stderr, "cannot access '%s': No such file\n",argv[0]);
        return 1;
    }

    //make groups ignoring LSBits
    // shift right
    int sr=QString(argv[2]).toInt(); //0 if no integer
    if (sr>15) {
        fprintf(stderr, "LSBits to ignore must be less than 16\n");
        return 1;
    }

    QApplication a(argc, argv);
    MainWindow w;

    SNDFILE* soundFileIn; /* Pointer for input sound file */
    SF_INFO soundInfoIn; /* Input sound file Info */

    soundInfoIn.format = 0;
    soundFileIn = sf_open(argv[1], SFM_READ, &soundInfoIn);

    if (soundFileIn == NULL){
        fprintf(stderr, "Could not open file for reading!!\n");
        return 1;
    }

    QVector<int> dataL,dataR,dataA;
    dataL.resize(MAX_SHORT>>sr);
    dataR.resize(MAX_SHORT>>sr);
    dataA.resize(MAX_SHORT>>sr);
    short sample[2];
    int n=0;
    while(sf_readf_short(soundFileIn, sample, 2) != 0) {
        dataL[(HALF_SHORT+sample[0])>>sr]+=1;
        dataR[(HALF_SHORT+sample[1])>>sr]+=1;
        dataA[(HALF_SHORT+(sample[0]+sample[1])/2)>>sr]+=1;
        n++;
    }
    sf_close(soundFileIn);

    double entropyL=0,entropyR=0,entropyA=0;
    for(int i=0;i<dataL.size();i++) {
        double p=(double)dataL[i]/n;
        if (p!=0.0)
            entropyL-=( p * log(p) );

        p=(double)dataR[i]/n;
        if (p!=0.0)
            entropyR-=( p * log(p) );

        p=(double)dataA[i]/n;
        if (p!=0.0)
            entropyA-=( p * log(p) );
    }

    HistogramWidget * h = static_cast <HistogramWidget*> (w.centralWidget());
    QString bn=QFileInfo(argv[1]).baseName();

    QList<QString> labels=QList<QString>()
                            <<bn+": Left: H="+QString::number(entropyL)
                            <<bn+": Rigth: H="+QString::number(entropyR)
                            <<bn+": Avg: H="+QString::number(entropyA);
    //histogram data
    h->setLabels(labels);
    h->setData(QList<QVector<int> >()   <<dataL     <<dataR     <<dataA);
    h->setColors(QList<QColor>()        <<Qt::blue  <<Qt::red   <<Qt::green);

    //print entropy on terminal
    foreach (const QString l, labels) {
        printf("%s\n",l.toUtf8().constData());
    }

    w.show();
    return a.exec();
}
