#pragma once

#include <QObject>
#include <QWidget>
#include <QKeyEvent>

static const QColor DEFAULT_COLOR=Qt::yellow;

class HistogramWidget : public QWidget
{
    Q_OBJECT
private:
    QList<QVector<int> > m_data;
    QList<QString> m_labels;
    QList<QColor> m_colors;
    QList<bool> m_visibility;

    int m_height_max;
    int m_force_height_max;
    int m_width;
    int m_sampling;

public:
    HistogramWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);

    void setData(const QList<QVector<int> >&);
    void setLabels(const QList<QString>&);
    void setColors(const QList<QColor>&);
    void setVisiblity(const QList<bool>&);

    QList<QVector<int> > data() const;
    QList<QString> labels() const;
    QList<QColor> colors() const;
    QList<bool> visiblity() const;

    void clear();

protected:
    void keyPressEvent(QKeyEvent* event);
    void paintEvent(QPaintEvent* event);
};
