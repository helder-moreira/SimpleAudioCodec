OBJS=$(patsubst %.cpp,%.o,$(wildcard BitStream/*.cpp) $(wildcard Predictors/*.cpp) $(wildcard Golomb/*.cpp))
CXX = g++ -Wall -std=c++0x -lsndfile
HISTOGRAM_DIR = Histogram
AUDIOCODEC_DIR = AudioCodec
BIN = $(AUDIOCODEC_DIR)/AudioCodec $(HISTOGRAM_DIR)/showHist

all: $(BIN)

$(HISTOGRAM_DIR)/*.cpp:

$(HISTOGRAM_DIR)/*.h:
	
$(HISTOGRAM_DIR)/Makefile:
	cd $(HISTOGRAM_DIR) && qmake

$(HISTOGRAM_DIR)/showHist: $(HISTOGRAM_DIR)/Makefile $(HISTOGRAM_DIR)/*.cpp $(HISTOGRAM_DIR)/*.h
	cd $(HISTOGRAM_DIR) && $(MAKE)

$(AUDIOCODEC_DIR)/*.cpp:

$(AUDIOCODEC_DIR)/*.h:
	
$(AUDIOCODEC_DIR)/Makefile:
	cd $(AUDIOCODEC_DIR) && qmake

$(AUDIOCODEC_DIR)/AudioCodec: $(AUDIOCODEC_DIR)/Makefile $(AUDIOCODEC_DIR)/*.cpp $(AUDIOCODEC_DIR)/*.h
	cd $(AUDIOCODEC_DIR) && $(MAKE)

%.x: %.cpp $(OBJS)
	$(CXX) -o $@ $^

%.o: %.cpp %.h $(OBJS)
	$(CXX) -c -o $@ $<

clean:
	rm $(BIN) || true
	cd $(HISTOGRAM_DIR) && qmake && $(MAKE) clean && rm Makefile
	cd $(AUDIOCODEC_DIR) && qmake && $(MAKE) clean && rm Makefile
