#-------------------------------------------------
#
# Project created by QtCreator 2015-11-02T23:46:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AudioCodec
TEMPLATE = app


SOURCES += main.cpp\
        ../Histogram/mainwindow.cpp\
        ../Histogram/histogramwidget.cpp\
        ../Predictors/*.cpp\
        ../BitStream/*.cpp\
        ../Golomb/*.cpp

HEADERS  += ../Histogram/mainwindow.h ../Histogram/histogramwidget.h

FORMS    += ../Histogram/mainwindow.ui

INCLUDEPATH += ../Histogram ../Predictors ../BitStream ../Golomb 

LIBS += -I../Histogram -I../Predictors -I../BitStream -I../Golomb -lsndfile

QMAKE_CXXFLAGS += -std=c++11
