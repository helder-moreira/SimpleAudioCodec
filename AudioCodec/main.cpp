#include "mainwindow.h"
#include <QApplication>
#include <QVector>


#include "MixedPredictor.h"
#include "MonkeysAudioPredictor.h"
#include "BitStream.h"
#include "Golomb.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sndfile.h>
#include <vector>
#include <time.h>
#include <getopt.h>
#include <sys/stat.h>
#include <math.h>

#define MAX_SHORT 65535
#define HALF_SHORT (int)(MAX_SHORT/2)

const int N_BITS = 32;

using namespace std;

Predictor *predictor;

int encode (QVector<int>&,QVector<int>&,int*);
int decode ();
void printUsage();
void printHeaderInfo(int, int, int, int, int, int);
void printProgress(int, int);
int parseOpts(int, char**);
long filesize(const char*);
bool estimateMs(vector< vector<int> >&,int*,int*);

int m1 = -1, m2 = -1, lossy = 0, operation = 0, predictor_code=0;
char* filein = NULL;
char* fileout = NULL;
bool plot = false, calc_psnr=false;

int main (int argc, char *argv[]) {
    if(parseOpts(argc, argv)){
        return -1;
    }
    if(operation == 1){
        // Vectors to store residuals
        QVector<int> left_residuals_hist;
        QVector<int> right_residuals_hist;
        int n=0;
        int r = encode(left_residuals_hist,right_residuals_hist,&n);

        if (plot) {

            double entropyL=0,entropyR=0;
            for(int i=0;i<left_residuals_hist.size();i++) {
                double p=(double)left_residuals_hist[i]/n;
                if (p!=0.0)
                    entropyL-=( p * log(p) );

                p=(double)right_residuals_hist[i]/n;
                if (p!=0.0)
                    entropyR-=( p * log(p) );

            }

            QApplication a(argc, argv);
            MainWindow w;

            HistogramWidget * h = static_cast <HistogramWidget*> (w.centralWidget());
            QString bn("Residuals");

            QList<QString> labels=QList<QString>()
                                    <<bn+": Left: H="+QString::number(entropyL)
                                    <<bn+": Rigth: H="+QString::number(entropyR);
            //histogram data
            h->setLabels(labels);
            h->setData(QList<QVector<int> >()   <<left_residuals_hist     <<right_residuals_hist);
            h->setColors(QList<QColor>()        <<Qt::blue  <<Qt::red );

            w.show();

            return a.exec();
        }

        return r;
    }else{
        return decode();
    }
}

int encode (QVector<int>& left_residuals_hist,QVector<int>& right_residuals_hist,int*total_samples) {
    //Variables to measure time elapsed on encoding
    clock_t tStart = clock();
    double elapsedtime=0;

    SNDFILE *soundFileIn; /* Pointer for input sound file */
    SF_INFO soundInfoIn; /* Input sound file Info */

    /* When opening a file for read, the format field should be set to zero
     * before calling sf_open(). All other fields of the structure are filled
     * in by the library
     */
    soundInfoIn.format = 0;
    soundFileIn = sf_open (filein, SFM_READ, &soundInfoIn);

    //Check if file was opened sucessfuly
    if (soundFileIn == NULL){
        cerr << "Error opening file '" << filein <<
            "' or file does not exist..." << endl;
        sf_close(soundFileIn);
        return -1;
    }

    //Init vectors for plot
    if(plot){
        left_residuals_hist.resize(MAX_SHORT<<2);
        right_residuals_hist.resize(MAX_SHORT<<2);
    }

    //Total number of frames
    int n_frames =(int) soundInfoIn.frames;

    switch(predictor_code){
        case 0: predictor = new MixedPredictor(lossy); break;
        case 1: predictor = new MonkeysAudioPredictor(); break;
    }

    vector< vector<int> > residuals;    //Vector to save residuals
    residuals.resize(n_frames);         //Init vector with n_frames

    short sample[2];                    //Array so save each read sample
    short decoded[2];                   //Array so save decoded when calc PSNR
    int coded[2];                       //Array to save residuals from sample
    sf_count_t nSamples = 1;            //Number of samples to read in each cycle

    float PSNR, MSE=0;

    cout << endl << "Encoding..." << endl;
    for (int i = 0; i < n_frames ; i++)
    {
        //Read sample
        if (sf_readf_short(soundFileIn, sample, nSamples) == 0){
            cerr << "Error: Reached end of file" << endl;
            sf_close(soundFileIn);
            break;
        }
        //Encode sample
        predictor->encode(sample, coded);

        //Calculate MSE for PSNR
        if(calc_psnr){
            predictor->decode(coded,decoded);
            MSE += ( pow((sample[LEFT_CHANNEL] - decoded[LEFT_CHANNEL]),2) +
                    pow((sample[RIGHT_CHANNEL] - decoded[RIGHT_CHANNEL]),2) );
        }

        //Save coded sample to vector
        residuals[i].resize(2);
        residuals[i][LEFT_CHANNEL] = coded[LEFT_CHANNEL];
        residuals[i][RIGHT_CHANNEL] = coded[RIGHT_CHANNEL];

        //Save values for plot
        if(plot){
            left_residuals_hist[(MAX_SHORT<<1)+coded[LEFT_CHANNEL]] += 1 ;
            right_residuals_hist[(MAX_SHORT<<1)+coded[RIGHT_CHANNEL]] += 1;
            (*total_samples)++;
        }
        printProgress(i+1, n_frames);
    }
    sf_close(soundFileIn);

    //Estimate M parameter for both channels (only if not defined by user)
    if(m1 < 0 || m2 < 0){
        estimateMs(residuals, &m1, &m2);
    }

    //Print Header Info
    printHeaderInfo(n_frames,soundInfoIn.samplerate,
                    soundInfoIn.channels,m1,m2,lossy);

    //Create bitstream and golomb encoder for each channel
    BitStream bitstream(fileout,"w");
    GolombEncoder golomb1(bitstream, m1, true);
    GolombEncoder golomb2(bitstream, m2, true);

    //Write header
    bitstream.WriteString(to_string(predictor_code));
    bitstream.WriteString(to_string(n_frames));
    bitstream.WriteString(to_string(soundInfoIn.samplerate));
    bitstream.WriteString(to_string(soundInfoIn.channels));
    bitstream.WriteString(to_string(soundInfoIn.format));
    bitstream.WriteString(to_string(m1));
    bitstream.WriteString(to_string(m2));
    bitstream.WriteString(to_string(lossy));

    cout << "Writting..." << endl;
    for (int i = 0; i < n_frames ; i++){
        golomb1.WriteInt(residuals[i][LEFT_CHANNEL]);
        golomb2.WriteInt(residuals[i][RIGHT_CHANNEL]);
        printProgress(i+1,n_frames);
    }
    bitstream.Close();

    //Print elapsed time and compression ratio
    elapsedtime= (double)(clock() - tStart)/CLOCKS_PER_SEC;
    double compress_ratio = 1.0 * filesize(filein) / filesize(fileout);
    printf("\nPerformance results:\n\tElapsed time\t  : %9.5f s\n\tCompression ratio : %9.5f\n",
        elapsedtime, compress_ratio);
    if(calc_psnr){
        MSE /= 2*n_frames;
        PSNR = (20 * log10(HALF_SHORT) - ( 10 * log10(MSE)) );
        printf("\tPSNR\t\t  : %9.5f dB\n", PSNR);
    }
    cout << endl;
    return 0;
}

int decode (){
    SNDFILE *soundFileOut; /* Pointer for output sound file */
    SF_INFO soundInfoOut; /* Output sound file Info */
    BitStream bitstream(filein,"r");
    int m1, m2, n_frames;

    //read header
    predictor_code = atoi(bitstream.ReadString().c_str());
    n_frames = atoi(bitstream.ReadString().c_str());
    soundInfoOut.samplerate = atoi(bitstream.ReadString().c_str());
    soundInfoOut.channels = atoi(bitstream.ReadString().c_str());
    soundInfoOut.format = atoi(bitstream.ReadString().c_str());
    m1=atoi(bitstream.ReadString().c_str());
    m2=atoi(bitstream.ReadString().c_str());
    lossy=atoi(bitstream.ReadString().c_str());

    // Open output file
    soundFileOut = sf_open (fileout, SFM_WRITE, &soundInfoOut);
    if (soundFileOut == NULL){
        cerr << "Error opening file '" << fileout << "' for writing" << endl;
        return -1;
    }

    //Print Header Info
    cout << "Getting header..." << endl;
    printHeaderInfo(n_frames,soundInfoOut.samplerate,
        soundInfoOut.channels,m1,m2,lossy);

    GolombDecoder golomb1(bitstream, m1,true);
    GolombDecoder golomb2(bitstream, m2,true);

    switch(predictor_code){
        case 0: predictor = new MixedPredictor(lossy); break;
        case 1: predictor = new MonkeysAudioPredictor(); break;
    }

    clock_t tStart = clock();
    double elapsedtime=0;

    cout << "Decoding..." << endl;
    int coded[2];
    short sample[2];
    sf_count_t nSamples = 1;

    for(int i = 0; i < n_frames; i++) {
        coded[LEFT_CHANNEL] = golomb1.ReadInt();
        coded[RIGHT_CHANNEL] = golomb2.ReadInt();
        predictor->decode(coded, sample);
        if (sf_writef_short(soundFileOut, sample, nSamples) != 1){
            cerr << "Error writing frames to the output" << endl;
          	sf_close(soundFileOut);
          	return -1;
	    }
        printProgress(i+1, n_frames);
    }
    bitstream.Close();
    sf_close(soundFileOut);

    elapsedtime= (double)(clock() - tStart)/CLOCKS_PER_SEC;
    printf("\nPerformance results:\n\tElapsed time: %9.5f s\n",elapsedtime);
    return 0;
}

bool estimateMs(vector< vector<int> >& vec, int *m1, int *m2){
    int temp, e1 = 0, e2 = 0;
    unsigned int i = 0;
    for(; i < vec.size(); i++){
        temp = vec[i][0];
        if(temp < 0){
            temp = (temp*-2) -1;
        }
        else{
            temp *= 2;
        }
        e1 += temp;
        temp = vec[i][1];
        if(temp < 0){
            temp = (temp*-2) -1;
        }
        else{
            temp *= 2;
        }
        e2 += temp;
    }
    e1 /= i;
    e2 /= i;

    *m1 = pow(2,ceil(log2(e1/2)));
    *m2 = pow(2,ceil(log2(e2/2)));
    if(*m1 == 0){
        (*m1)++;
    }
    if(*m2 == 0){
        (*m2)++;
    }
    return true;
}

void printProgress(int n, int total){
    if( n % (total/100) == 0 ){
        int percent =  (n*100.0/total) +1;
        fprintf(stderr, "\r   Progress: [%s%s] %d%%",
            string( percent/2 ,'#').c_str(),
            string( 50-(percent/2) ,'-').c_str(),
            percent);
        if (percent == 100){
            fprintf(stderr, "\n");
        }
    }
}

void printHeaderInfo(int f, int s, int c, int m1, int m2, int l){
    printf(
        ".......................................\n"
        "Header info:\n"
        "   Frames (samples): %d\n"
        "   Samplerate      : %d\n"
        "   Channels        : %d\n"
        "   M for channel 1 : %d\n"
        "   M for channel 2 : %d\n"
        "   Lossy factor    : %d\n"
        ".......................................\n\n",
        f,s,c,m1,m2,l);
}

void printUsage(){
    fprintf(stderr, "Usage:\n\t./AudioCodec.x "
    "-e/-d -i <input file> -o <output file> [-f m1] [-s m2] [-l factor] [-p] [-c] [-h] [-m predictor]\n\n"
    "\t-e\t: Encode operation\n"
    "\t-d\t: Decode operation\n"
    "\t-m\t: Change predictor. Available predictors are: { mixed, monkeys } (Default is mixed)\n"
    "\t-i\t: Set input file\n"
    "\t-o\t: Set output file\n"
    "\t-f\t: Set first M parameter for Golomb (left channel)\n"
    "\t-s\t: Set second M parameter for Golomb (right channel)\n"
    "\t-l\t: Enable lossy and set its factor.\n"
    "\t-p\t: Shows plot\n"
    "\t-c\t: Calculate PSNR\n"
    "\t-h\t: Shows this menu\n\n");
}

int parseOpts(int argc, char** argv) {
    int c;
    while ((c = getopt(argc, argv, "i:o:f:s:l:pchdem:")) != -1) {
        switch (c) {
            case 'd':
                operation = 2;
                break;
            case 'e':
            	operation = 1;
            	break;
            case 'l':
            	lossy = atoi(optarg);
            	break;
            case 'f':
            	m1 = atoi(optarg);
            	break;
            case 's':
            	m2 = atoi(optarg);
            	break;
            case 'i':
                filein = optarg;
                break;
            case 'o':
                fileout = optarg;
                break;
            case 'p':
				plot=true;
                break;
            case 'm':
                if(strcmp(optarg, "mixed") == 0){
                    predictor_code = 0;
                }
                else if (strcmp(optarg, "monkeys") == 0){
                    predictor_code = 1;
                }else{
                    printUsage();
                    return -1;
                }
                break;
            case 'c':
    			calc_psnr=true;
                break;
            case 'h':
            default:
                printUsage();
                return -1;
        }
    }
    if(filein == NULL || fileout == NULL || operation == 0){
        printUsage();
        return -1;
    }
    return 0;
}

long filesize(const char* filename)
{
    struct stat stat_buf;
    int rc = stat(filename, &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}
