#ifndef PREDICTOR_H
#define PREDICTOR_H

#define LEFT_CHANNEL 0
#define RIGHT_CHANNEL 1

class Predictor {
    public:
        Predictor () { }       /*Constructor*/
        virtual ~Predictor() { }
        virtual bool encode(short[2], int[2]) = 0;  /*Encode*/
        virtual bool decode(int[2], short[2]) = 0;  /*Decode*/
        virtual bool setLossy(int) = 0;      /*Set lossy*/
};

#endif
