#ifndef MIXED_PREDICTOR_H
#define MIXED_PREDICTOR_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "Predictor.h"

using namespace std;

class MixedPredictor : public Predictor {
    public:
        MixedPredictor() : m_lossy(0) {
            reset();
        }
        MixedPredictor(int lossy) : m_lossy(lossy){
            reset();
        }
        ~MixedPredictor() { }
        bool encode(short[2], int[2]);
        bool decode(int[2], short[2]);
        void reset();
        bool setLossy(int lossy) {
            m_lossy = lossy;
            return true;
        }
    private:
        int mapPositive(int n);
        int restoreValue(int n);

        int m_lossy;
        int m_previous_e[2];
        int m_previous_d[2];
};

#endif
