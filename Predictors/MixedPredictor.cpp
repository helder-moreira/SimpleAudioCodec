#include "MixedPredictor.h"

#define MAX_SHORT 65535
#define HALF_SHORT (int)(MAX_SHORT/2)

bool MixedPredictor::encode(short sample[2], int coded[2]){
    int L = sample[LEFT_CHANNEL], R = sample[RIGHT_CHANNEL];
    int PX = m_previous_e[LEFT_CHANNEL], PY = m_previous_e[RIGHT_CHANNEL];
    
    //Map Values to Positive
    L = mapPositive(L);
    R = mapPositive(R);

    //Channel Redundancy
    int X = (L + R)/2;
    int Y = L - R;

    //Quantization for lossy
    if(m_lossy){
        X /= m_lossy;
        Y /= m_lossy;
    }

    //Temporal Redundancy
    int RL = X - PX;
    int RR = Y - PY;

    //Save X and Y to previous
    m_previous_e[LEFT_CHANNEL] = X;
    m_previous_e[RIGHT_CHANNEL] = Y;

    coded[LEFT_CHANNEL] = RL;
    coded[RIGHT_CHANNEL] = RR;
    return true;
}

bool MixedPredictor::decode(int coded[2], short sample[2]){
    int RL = coded[LEFT_CHANNEL], RR = coded[RIGHT_CHANNEL];
    int PX = m_previous_d[LEFT_CHANNEL], PY = m_previous_d[RIGHT_CHANNEL];
    
    //Temporal Redundancy
    float X = PX + RL;
    int Y = PY + RR;

    //Save M and S to previous
    m_previous_d[LEFT_CHANNEL] = X;
    m_previous_d[RIGHT_CHANNEL] = Y;
    
    //Quantization for lossy
    if(m_lossy){
        X *= m_lossy;
        Y *= m_lossy;
    }
    
    //Channel Redundancy
    if(Y%2 != 0){
        X += 0.5;
    }
    int L = (2*X + Y)/2;
    int R = L - Y;

    //Restore Values to their original Signal
    L = restoreValue(L);
    R = restoreValue(R);

    sample[LEFT_CHANNEL] = L;
    sample[RIGHT_CHANNEL] = R;

    return true;
}

void MixedPredictor::reset(){
    m_previous_e[LEFT_CHANNEL] = m_previous_e[RIGHT_CHANNEL]
    = m_previous_d[LEFT_CHANNEL] = m_previous_d[RIGHT_CHANNEL] = 0;
}

int MixedPredictor::mapPositive(int n){
    return n+(HALF_SHORT);
}

int MixedPredictor::restoreValue(int n){
    return n-(HALF_SHORT);
}
