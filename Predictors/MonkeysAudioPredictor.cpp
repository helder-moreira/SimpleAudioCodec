#include "MonkeysAudioPredictor.h"

#define MAX_SHORT 65535
#define HALF_SHORT (int)(MAX_SHORT/2)

bool MonkeysAudioPredictor::encode(short sample[2], int coded[2]){
    int L = sample[LEFT_CHANNEL], R = sample[RIGHT_CHANNEL];
    int PX1 = m_previous_e1[LEFT_CHANNEL], PY1 = m_previous_e1[RIGHT_CHANNEL];
    int PX2 = m_previous_e2[LEFT_CHANNEL], PY2 = m_previous_e2[RIGHT_CHANNEL];
    
    //Map Values to Positive
    L = mapPositive(L);
    R = mapPositive(R);

    //Channel Redundancy
    int X = (L + R)/2;
    int Y = L - R;

    //Temporal Redundancy
    int PX = (2*PX1) - PX2;
    int PY = (2*PY1) - PY2;

    int RL = X - PX;
    int RR = Y - PY;

    //Save X and Y to previous
    m_previous_e1[LEFT_CHANNEL] = X;
    m_previous_e1[RIGHT_CHANNEL] = Y;
    m_previous_e2[LEFT_CHANNEL] = PX1;
    m_previous_e2[RIGHT_CHANNEL] = PY1;

    coded[LEFT_CHANNEL] = RL;
    coded[RIGHT_CHANNEL] = RR;
    return true;
}

bool MonkeysAudioPredictor::decode(int coded[2], short sample[2]){
    int RL = coded[LEFT_CHANNEL], RR = coded[RIGHT_CHANNEL];
    int PX1 = m_previous_d1[LEFT_CHANNEL], PY1 = m_previous_d1[RIGHT_CHANNEL];
    int PX2 = m_previous_d2[LEFT_CHANNEL], PY2 = m_previous_d2[RIGHT_CHANNEL];
    
    int PX = (2*PX1) - PX2;
    int PY = (2*PY1) - PY2;

    //Temporal Redundancy
    float X = PX + RL;
    int Y = PY + RR;

    //Save X and Y to previous
    m_previous_d1[LEFT_CHANNEL] = X;
    m_previous_d1[RIGHT_CHANNEL] = Y;
    m_previous_d2[LEFT_CHANNEL] = PX1;
    m_previous_d2[RIGHT_CHANNEL] = PY1;

    //Channel Redundancy
    if(Y%2 != 0){
        X += 0.5;
    }
    int L = (2*X + Y)/2;
    int R = L - Y;

    //Restore Values to their original signal
    L = restoreValue(L);
    R = restoreValue(R);

    sample[LEFT_CHANNEL] = L;
    sample[RIGHT_CHANNEL] = R;

    return true;
}

void MonkeysAudioPredictor::reset(){
        m_previous_e1[LEFT_CHANNEL] = m_previous_e1[RIGHT_CHANNEL]
        = m_previous_d1[LEFT_CHANNEL] = m_previous_d1[RIGHT_CHANNEL] = 0;
        m_previous_e2[LEFT_CHANNEL] = m_previous_e2[RIGHT_CHANNEL]
        = m_previous_d2[LEFT_CHANNEL] = m_previous_d2[RIGHT_CHANNEL] = 0;
}

int MonkeysAudioPredictor::mapPositive(int n){
    return n+(HALF_SHORT);
}

int MonkeysAudioPredictor::restoreValue(int n){
    return n-(HALF_SHORT);
}
