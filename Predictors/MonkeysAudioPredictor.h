#ifndef MONKEYS_AUDIO_PREDICTOR_H
#define MONKEYS_AUDIO_PREDICTOR_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "Predictor.h"

using namespace std;

class MonkeysAudioPredictor : public Predictor {
    public:
        MonkeysAudioPredictor() {
            reset();
        }
        ~MonkeysAudioPredictor() { }
        bool encode(short[2], int[2]);
        bool decode(int[2], short[2]);
        bool setLossy(int) {
            return false;
        }
        void reset();
    private:
        int mapPositive(int n);
        int restoreValue(int n);

        int m_previous_e1[2];
        int m_previous_e2[2];

        int m_previous_d1[2];
        int m_previous_d2[2];
};

#endif
