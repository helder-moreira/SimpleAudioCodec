#include <iostream>
#include <cmath>
#include "Golomb.h"

using namespace std;

GolombEncoder::GolombEncoder(BitStream & bstream, unsigned m,bool neg) {
    this->bitstream = &bstream;
	this->m = m;
    this->negatives = neg;
	b = (int)ceil(log2(m));
	l = (0x01 << b) - m;
}

void GolombEncoder::SetParameter(unsigned m) {
	this->m = m;
	b = (int)ceil(log2(m));
	l = (0x01 << b) - m;
}

void GolombEncoder::WriteInt(int x) {
    if (negatives){
        if (x<0) {//negative
            x=x*-2-1;
        } else {//positive
            x*=2;
        }
    }
	unsigned q = x / m;
	unsigned r = x % m;

	for(unsigned i = 0 ; i < q ; i++)
		bitstream->WriteBit(0);

	bitstream->WriteBit(1); // write the comma of the unary code

	if(r < l)
		bitstream->WriteNBits(r, b - 1);

	else
		bitstream->WriteNBits(r + l, b);

}

GolombDecoder::GolombDecoder(BitStream & bstream, unsigned m, bool neg) {
    this->bitstream = &bstream;
	this->m = m;
    this->negatives = neg;
	b = (int)ceil(log2(m));
	l = (0x01 << b) - m;
}

void GolombDecoder::SetParameter(unsigned m) {
	this->m = m;
	b = (int)ceil(log2(m));
	l = (0x01 << b) - m;
}

int GolombDecoder::ReadInt(void) {
	unsigned q = 0, r;

	while(bitstream->ReadBit() != 1)
		q++;

	if(m == 1) // In this particular case, there is no remainder
		return (unsigned)q;

	unsigned x = bitstream->ReadNBits(b - 1);

	if(x < l) {
		r = x;
	}

	else {
		x = (x << 1) | bitstream->ReadBit();
		r = x - l;
	}
    x = (unsigned)(q * m + r);

    if (negatives) {
        if (x%2==1){ //negative
            return 0-(x/2)-1;
        } else {//positive
            return x/2;
        }
    } else {
        return x;
    }
}
